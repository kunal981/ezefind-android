package com.appalmighty.ezefind.newentry.act;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.R;

public class EditItemsActivity extends Activity {
	ListView listview_edit;
	ListAdapter adapteredit;
	Button backButton, btnLogout;
	ArrayList<String> arrayList;
	RelativeLayout rel_popup;
	LinearLayout linearYes, linearNo;
	ImageButton imgbtnCancel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_items);
		backButton = (Button) findViewById(R.id.back_button);
		btnLogout = (Button) findViewById(R.id.btn_logout);
		listview_edit = (ListView) findViewById(R.id.listview_edit_items);
		rel_popup = (RelativeLayout) findViewById(R.id.container_pop);
		linearYes = (LinearLayout) findViewById(R.id.linearYes);
		linearNo = (LinearLayout) findViewById(R.id.linearlayoutNo);
		imgbtnCancel = (ImageButton) findViewById(R.id.imgbtn_cancel);
		arrayList = new ArrayList<String>();
		arrayList.add("1");
		arrayList.add("2");
		arrayList.add("3");
		arrayList.add("4");
		arrayList.add("5");
		arrayList.add("6");

		adapteredit = new ListAdapter(arrayList, getApplicationContext());
		listview_edit.setAdapter(adapteredit);
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		btnLogout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rel_popup.setVisibility(View.VISIBLE);
			}
		});
		linearYes.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearYes.setSelected(true);
				linearNo.setSelected(false);
			}
		});
		linearNo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearYes.setSelected(false);
				linearNo.setSelected(true);

			}
		});
		imgbtnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rel_popup.setVisibility(View.GONE);
			}
		});

	}

	// private void addClickListernerToList() {
	//
	// listview_edit.setOnItemLongClickListener(new OnItemLongClickListener() {
	//
	// @Override
	// public boolean onItemLongClick(AdapterView<?> parent, View view,
	// int position, long id) {
	//
	// View view2 = listview_edit.getChildAt(position);
	//
	// view2.findViewById(R.id.btn_open_zip).setVisibility(View.GONE);
	// view2.findViewById(R.id.btn_quantity).setVisibility(View.GONE);
	// view2.findViewById(R.id.btn_edit).setVisibility(View.VISIBLE);
	// view2.findViewById(R.id.btn_delete).setVisibility(View.VISIBLE);
	//
	// return false;
	// }
	// });
	//
	// }

	class ListAdapter extends BaseAdapter {

		private LayoutInflater inflater;
		ArrayList<String> arrList = new ArrayList<String>();
		Context context;

		public ListAdapter(ArrayList<String> arrList, Context context) {

			this.arrList = arrList;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arrList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return arrList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;

			if (inflater == null)
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.activity_edititems_list, null);
				holder = new ViewHolder();

				holder.parentLayout = (RelativeLayout) convertView
						.findViewById(R.id.relative_parent_);
				holder.btnOpen = (Button) convertView
						.findViewById(R.id.btn_open_zip_);
				holder.btnQuantity = (Button) convertView
						.findViewById(R.id.btn_quantity_);
				holder.btnEdit = (Button) convertView
						.findViewById(R.id.btn_edit_);
				holder.btnDelete = (Button) convertView
						.findViewById(R.id.btn_delete);
				holder.edtTitle = (EditText) convertView
						.findViewById(R.id.edit_shirts_name);

				holder.edtDesc = (EditText) convertView
						.findViewById(R.id.edt_descriptions);
				holder.imgIcon = (ImageView) convertView
						.findViewById(R.id.img_shirts__);
				holder.layout_edittexts = (LinearLayout) convertView
						.findViewById(R.id.linear_layout_e);
				holder.txtTitle = (TextView) convertView
						.findViewById(R.id.tv_shirts_name);
				holder.txtDesc = (TextView) convertView
						.findViewById(R.id.tv_description);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			// holder.btnOpen.setVisibility(View.VISIBLE);
			// holder.btnQuantity.setVisibility(View.VISIBLE);
			// holder.btnEdit.setVisibility(View.GONE);
			// holder.btnDelete.setVisibility(View.GONE);
			// holder.edtTitle.setSelected(false);
			// holder.edtDesc.setSelected(false);
			// holder.imgIcon.setSelected(false);

			// holder.parentLayout
			// .setOnLongClickListener(new OnLongClickListener() {
			//
			// @Override
			// public boolean onLongClick(View v) {
			// // TODO Auto-generated method stub
			// holder.btnOpen.setVisibility(View.GONE);
			// holder.btnQuantity.setVisibility(View.GONE);
			// holder.btnEdit.setVisibility(View.VISIBLE);
			// holder.btnDelete.setVisibility(View.VISIBLE);
			// return false;
			// }
			// });
			holder.parentLayout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					holder.btnOpen.setVisibility(View.GONE);
					holder.btnQuantity.setVisibility(View.GONE);
					holder.btnEdit.setVisibility(View.VISIBLE);
					holder.btnDelete.setVisibility(View.VISIBLE);
					holder.layout_edittexts.setVisibility(View.VISIBLE);
				}
			});

			holder.btnEdit.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					holder.edtTitle.setVisibility(View.VISIBLE);
					holder.edtDesc.setVisibility(View.VISIBLE);
					holder.txtDesc.setVisibility(View.GONE);
					holder.txtTitle.setVisibility(View.GONE);
					holder.edtTitle.setSelected(true);
					holder.edtDesc.setSelected(true);
					holder.imgIcon.setSelected(true);
					Log.e("clicked:", "edit button");

				}
			});

			holder.btnDelete.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					arrList.remove(arrList.get(position));

					adapteredit.notifyDataSetChanged();

				}
			});

			return convertView;
		}
	}

	class ViewHolder {
		Button btnOpen, btnQuantity, btnEdit, btnDelete;
		RelativeLayout parentLayout;
		LinearLayout layout_edittexts;
		EditText edtTitle, edtDesc;
		TextView txtTitle, txtDesc;
		ImageView imgIcon;

	}

}

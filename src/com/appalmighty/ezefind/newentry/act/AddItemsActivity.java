package com.appalmighty.ezefind.newentry.act;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.util.Helper.ToastUi;

public class AddItemsActivity extends Activity implements OnClickListener {

	public static final int REQUEST_AUDIO_TO_TEXT_TITLE = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_DESC = 303;
	MediaRecorder myAudioRecorder = null;
	MediaPlayer myMediaPlayer = null;
	ListView listview_items;
	ListAdapter adapter1;
	Button btn_add, backButton;;
	RelativeLayout relative_addItems;
	ImageView img_save;
	private LinearLayout layout_addItem_box_audio;
	private LinearLayout layout_addItem_box_text;
	private int editItemMode = 0; // 0 for text by default 1 for audio
	private TextView textAudioTitle, textAudioDesc;
	private ImageView imageButtonAttachement, imageButtonQuantity;

	public static int countQuantity = 0;
	String outputFile = null;
	String AUDIO_RECORDER_FOLDER = "eze-find";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_items);
		listview_items = (ListView) findViewById(R.id.listview_add_items);
		adapter1 = new ListAdapter(this);
		listview_items.setAdapter(adapter1);
		textAudioTitle = (TextView) findViewById(R.id.edt_shirts_name_audio);
		textAudioDesc = (TextView) findViewById(R.id.edt_desc_audio);
		btn_add = (Button) findViewById(R.id.btn_addMore);
		img_save = (ImageView) findViewById(R.id.img_btn_save);
		backButton = (Button) findViewById(R.id.back_button);
		relative_addItems = (RelativeLayout) findViewById(R.id.relative_layout);
		layout_addItem_box_audio = (LinearLayout) findViewById(R.id.linear_layout_mode_audio);
		layout_addItem_box_text = (LinearLayout) findViewById(R.id.linear_layout_mode_text);
		imageButtonAttachement = (ImageView) findViewById(R.id.imageButtonAttachment);
		imageButtonQuantity = (ImageView) findViewById(R.id.imageButtonQuantity);

		btn_add.setOnClickListener(this);
		img_save.setOnClickListener(this);
		textAudioTitle.setOnClickListener(this);
		textAudioDesc.setOnClickListener(this);
		imageButtonQuantity.setOnClickListener(this);
		imageButtonAttachement.setOnClickListener(this);

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	class ListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;

		public ListAdapter(Activity activity) {
			super();
			// TODO Auto-generated constructor stub
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 4;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_on_open_listitems, null);

			}
			return convertView;

		}
	}

	protected void openTypeModePopWindow() {
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_option_text_mode, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		ImageView btnDismiss = (ImageView) popupView
				.findViewById(R.id.btn_close);
		btnDismiss.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				popupWindow.dismiss();
			}
		});
		Button audioMode = (Button) popupView.findViewById(R.id.btn_mode_audio);
		audioMode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				editItemMode = 1;
				openAudioModeEditbox();
				popupWindow.dismiss();
			}
		});
		Button textmode = (Button) popupView.findViewById(R.id.btn_mode_text);
		textmode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				editItemMode = 0;
				openTextModeEditbox();
				popupWindow.dismiss();
			}
		});

		popupWindow.showAtLocation(layout_addItem_box_audio,
				Gravity.CENTER_HORIZONTAL, 0, 0);

	}

	protected void openAudioModeEditbox() {
		layout_addItem_box_audio.setVisibility(View.VISIBLE);
		layout_addItem_box_text.setVisibility(View.GONE);
	}

	protected void openTextModeEditbox() {
		layout_addItem_box_audio.setVisibility(View.GONE);
		layout_addItem_box_text.setVisibility(View.VISIBLE);
	}

	private void publishQuantityPopupWindow() {
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.popup_layout_quantity,
				null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		Button incrementOp = (Button) popupView
				.findViewById(R.id.btn_increment);
		final Button quantity = (Button) popupView
				.findViewById(R.id.btn_quantity);
		Button decrementOp = (Button) popupView
				.findViewById(R.id.btn_decrement);
		quantity.setText(String.valueOf(countQuantity));
		incrementOp.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				countQuantity = countQuantity + 1;
				quantity.setText(String.valueOf(countQuantity));

			}
		});
		decrementOp.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (countQuantity != 0)
					countQuantity = countQuantity - 1;

				quantity.setText(String.valueOf(countQuantity));

			}
		});
		quantity.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

			}
		});
		// popupWindow.showAtLocation(layout_addItem_box_audio, Gravity.CENTER,
		// 0,
		// 0);
		popupWindow.showAsDropDown(imageButtonAttachement, 0, 0);
	}

	private void openItemAttachMenu() {
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_attachement, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		Button optionphotoVideo = (Button) popupView
				.findViewById(R.id.btn_photo_video);
		final Button optionphotoAudio = (Button) popupView
				.findViewById(R.id.btn_audio);
		Button optionBarcode = (Button) popupView
				.findViewById(R.id.btn_barcode);
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);
		optionphotoVideo.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				openCameraOptionDailog();
				popupWindow.dismiss();

			}
		});
		optionphotoAudio.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				openAudioOptionDailog();
				popupWindow.dismiss();

			}
		});
		optionBarcode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				ToastUi.print(AddItemsActivity.this, "Coming soon");

			}
		});
		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

			}
		});
		popupWindow.showAtLocation(layout_addItem_box_audio, Gravity.CENTER, 0,
				0);
		// popupWindow.showAsDropDown(imageButtonAttachement, 0, 0);
	}

	private void openAudioOptionDailog() {

		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_audio_recording, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);
		ImageView btnSave = (ImageView) popupView
				.findViewById(R.id.id_btn_save);
		ImageView btnDel = (ImageView) popupView.findViewById(R.id.id_btn_del);
		final Chronometer focus = (Chronometer) popupView
				.findViewById(R.id.chronometer1);

		final TextView duration = (TextView) popupView
				.findViewById(R.id.duration);
		final ImageView btn_play_pause = (ImageView) popupView
				.findViewById(R.id.id_btn_record);
		final ImageView btn_play_record = (ImageView) popupView
				.findViewById(R.id.imageView1);

		final Handler seekHandler = new Handler();

		final SeekBar seekBar = (SeekBar) popupView.findViewById(R.id.seek_bar);

		btn_play_pause.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!btn_play_pause.isSelected()) {
					// to start audio recording

					initMediaRecorder();
					start(v);

				} else {
					// to stop audio recording
					stop(v);
				}

			}

			private void stop(View v) {
				myAudioRecorder.stop();
				myAudioRecorder.release();
				focus.stop();

				myAudioRecorder = null;
				btn_play_pause.setSelected(false);
				Toast.makeText(getApplicationContext(),
						"Audio recorded successfully", Toast.LENGTH_LONG)
						.show();
			}

			private void start(View v) {
				try {
					myAudioRecorder.prepare();
					myAudioRecorder.start();
					btn_play_pause.setSelected(true);
					Toast.makeText(getApplicationContext(),
							"Recording started", Toast.LENGTH_LONG).show();
					focus.start();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					btn_play_pause.setSelected(false);
					Toast.makeText(getApplicationContext(), "Recording falied",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					btn_play_pause.setSelected(false);
					Toast.makeText(getApplicationContext(), "Recording falied",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}

			}

			private void initMediaRecorder() {
				myAudioRecorder = new MediaRecorder();
				myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				myAudioRecorder
						.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
				myAudioRecorder
						.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
				outputFile = getFilename();
				myAudioRecorder.setOutputFile(outputFile);
			}

			private String getFilename() {
				String filepath = Environment.getExternalStorageDirectory()
						.getPath();
				File file = new File(filepath, AUDIO_RECORDER_FOLDER);
				if (!file.exists()) {
					file.mkdirs();
				}
				String path = file.getAbsolutePath() + "/"
						+ System.currentTimeMillis() + ".3gp";
				return path;
			}
		});

		btn_play_record.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!btn_play_record.isSelected()) {

					startPlaying();

				} else {
					stopPlayingRecord();

				}

			}

			private void stopPlayingRecord() {
				myMediaPlayer.stop();
				myMediaPlayer.release();
				myMediaPlayer = null;
				btn_play_record.setSelected(false);
			}

			private void startPlaying() {
				// TODO Auto-generated method stub

				if (outputFile != null && !outputFile.equals("")) {
					if (myMediaPlayer == null) {
						myMediaPlayer = new MediaPlayer();
					}

					try {
						myMediaPlayer.setDataSource(outputFile);// Write your
						// location

						myMediaPlayer.prepare();
						myMediaPlayer.start();

						seekBar.setMax(myMediaPlayer.getDuration());
						int mediaDuration = myMediaPlayer.getDuration();
						int mediaPosition = myMediaPlayer.getCurrentPosition();
						double timeRemaining = mediaDuration - mediaPosition;
						duration.setText(String.format(
								"%d min, %d sec",
								TimeUnit.MILLISECONDS
										.toMinutes((long) timeRemaining),
								TimeUnit.MILLISECONDS
										.toSeconds((long) timeRemaining)
										- TimeUnit.MINUTES
												.toSeconds(TimeUnit.MILLISECONDS
														.toMinutes((long) timeRemaining))));

						btn_play_record.setSelected(true);
						seekUpdation();
					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(AddItemsActivity.this, "File not exist",
								Toast.LENGTH_SHORT).show();
					}

				} else {
					Toast.makeText(AddItemsActivity.this, "File not exist",
							Toast.LENGTH_SHORT).show();
				}

			}

			public void seekUpdation() {
				if (myMediaPlayer != null) {
					seekBar.setProgress(myMediaPlayer.getCurrentPosition());
					seekHandler.postDelayed(run, 1000);
				} else {
					seekBar.setProgress(0);
				}
			}

			Runnable run = new Runnable() {
				@Override
				public void run() {
					seekUpdation();
				}
			};

		});

		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (myAudioRecorder != null) {
					myAudioRecorder.stop();
					myAudioRecorder.release();
					focus.stop();
					myAudioRecorder = null;
				}
				if (myMediaPlayer != null) {
					myMediaPlayer.stop();
					myMediaPlayer.release();
					myMediaPlayer = null;
				}
				popupWindow.dismiss();

			}
		});
		btnSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(AddItemsActivity.this,
						"Successfully Save the file", Toast.LENGTH_SHORT)
						.show();

			}
		});
		btnDel.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				deleteFile();

			}

			private void deleteFile() {
				if (outputFile != null && !outputFile.equals("")) {
					File file = new File(outputFile);
					if (file != null) {
						if (file.exists()) {
							file.delete();
							Toast.makeText(AddItemsActivity.this,
									"Audio Deleted successfully",
									Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(AddItemsActivity.this,
									"File not exit ", Toast.LENGTH_SHORT)
									.show();
						}
					}

				} else {
					Toast.makeText(AddItemsActivity.this, "File not exit ",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		popupWindow.showAtLocation(layout_addItem_box_audio, Gravity.CENTER, 0,
				0);
	}

	private void openCameraOptionDailog() {

		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_caputre_selection, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		Button camera = (Button) popupView
				.findViewById(R.id.id_button_take_camera);
		Button gallery = (Button) popupView.findViewById(R.id.id_button_save);
		camera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ToastUi.print(AddItemsActivity.this, "Coming soon");
				popupWindow.dismiss();

			}
		});
		gallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ToastUi.print(AddItemsActivity.this, "Coming soon");
				popupWindow.dismiss();

			}
		});
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);
		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

			}
		});
		popupWindow.showAtLocation(layout_addItem_box_audio, Gravity.CENTER, 0,
				0);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int ID = v.getId();
		switch (ID) {
		case R.id.btn_addMore:
			relative_addItems.setVisibility(View.VISIBLE);
			openTypeModePopWindow();
			break;
		case R.id.img_btn_save:
			btn_add.setVisibility(View.VISIBLE);
			relative_addItems.setVisibility(View.GONE);
			break;
		case R.id.edt_shirts_name_audio:
			if (isSpeechRecognitionActivityPresented(AddItemsActivity.this) == true) {
				// if yes  running recognition
				startRecognition(AddItemsActivity.this,
						REQUEST_AUDIO_TO_TEXT_TITLE);
			} else {
				// if no, then showing notification to install Voice Search
				Toast.makeText(
						AddItemsActivity.this,
						"In order to activate speech recognition you must install Google Voice Search",
						Toast.LENGTH_LONG).show();
				// start installing process
				installGoogleVoiceSearch(AddItemsActivity.this);
			}
			break;
		case R.id.edt_desc_audio:
			if (isSpeechRecognitionActivityPresented(AddItemsActivity.this) == true) {
				// if yes  running recognition
				startRecognition(AddItemsActivity.this,
						REQUEST_AUDIO_TO_TEXT_DESC);
			} else {
				// if no, then showing notification to install Voice Search
				Toast.makeText(
						AddItemsActivity.this,
						"In order to activate speech recognition you must install Google Voice Search",
						Toast.LENGTH_LONG).show();
				// start installing process
				installGoogleVoiceSearch(AddItemsActivity.this);
			}
			break;

		case R.id.imageButtonQuantity:
			publishQuantityPopupWindow();
			break;
		case R.id.imageButtonAttachment:
			openItemAttachMenu();
			break;
		default:
			break;
		}

	}

	protected void startRecognition(Activity mainActivity, int RequestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		startActivityForResult(intent, RequestCode);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case REQUEST_AUDIO_TO_TEXT_TITLE:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				textAudioTitle.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_DESC:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				textAudioDesc.setText(text.get(0));
			}
			break;
		}

	}

	/**
	 * Checks availability of speech recognizing Activity
	 *
	 * @param callerActivity
	 *             Activity that called the checking
	 * @return true  if Activity there available, false  if Activity is absent
	 */
	private static boolean isSpeechRecognitionActivityPresented(
			Activity callerActivity) {
		try {
			// getting an instance of package manager
			PackageManager pm = callerActivity.getPackageManager();
			// a list of activities, which can process speech recognition Intent
			List activities = pm.queryIntentActivities(new Intent(
					RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);

			if (activities.size() != 0) { // if list not empty
				return true; // then we can recognize the speech
			}
		} catch (Exception e) {

		}

		return false; // we have no activities to recognize the speech
	}

	/**
	 * Asking the permission for installing Google Voice Search. If permission
	 * granted  sent user to Google Play
	 * 
	 * @param callerActivity
	 *             Activity, that initialized installing
	 */
	private static void installGoogleVoiceSearch(final Activity ownerActivity) {

		// creating a dialog asking user if he want
		// to install the Voice Search
		Dialog dialog = new AlertDialog.Builder(ownerActivity)
				.setMessage(
						"For recognition its necessary to install Google Voice Search") // dialog
																							// message
				.setTitle("Install Voice Search from Google Play?") // dialog
																	// header
				.setPositiveButton("Install",
						new DialogInterface.OnClickListener() { // confirm
							// button

							// Install Button click handler
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								try {
									// creating an Intent for opening
									// applications page in Google Play
									// Voice Search package name:
									// com.google.android.voicesearch
									Intent intent = new Intent(
											Intent.ACTION_VIEW,
											Uri.parse("market://details?id=com.google.android.voicesearch"));
									// setting flags to avoid going in
									// application history (Activity call stack)
									intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
											| Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
									// sending an Intent
									ownerActivity.startActivity(intent);
								} catch (Exception ex) {
									// if something going wrong
									// doing nothing
								}
							}
						})

				.setNegativeButton("Cancel", null) // cancel button
				.create();

		dialog.show(); // showing dialog
	}

}

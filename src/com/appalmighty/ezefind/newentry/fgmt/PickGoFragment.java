package com.appalmighty.ezefind.newentry.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.R;

public class PickGoFragment extends Fragment {

	public static final String TAG = PickGoFragment.class.getName();

	private ListView listView;
	private ListAdapter adapter;
	private Button btn_start_new;
	private boolean clicked = true;
	private TextView txtTitle;
	public static final String KEY = "key";
	String key, strKey;

	public PickGoFragment() {
	}

	public static PickGoFragment newInstance(String key) {
		PickGoFragment fragment = new PickGoFragment();
		Bundle bundle = new Bundle();
		bundle.putString(KEY, key);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home_pack_go,
				container, false);
		btn_start_new = (Button) rootView
				.findViewById(R.id.id_button_start_new_package);
		listView = (ListView) rootView.findViewById(R.id.listView_items);
		adapter = new ListAdapter(getActivity());
		listView.setAdapter(adapter);
		txtTitle = (TextView) rootView.findViewById(R.id.txt_title_);
		intHeaderTitle();
		btn_start_new.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartPackingFragment fragment = new StartPackingFragment();

				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				ft.replace(R.id.container, fragment);
				Bundle bundle = new Bundle();
				bundle.putString(KEY, key);
				fragment.setArguments(bundle);
				ft.addToBackStack(null);
				ft.commit();
			}
		});

		return rootView;
	}

	private void intHeaderTitle() {
		// TODO Auto-generated method stub
		key = getArguments().getString(KEY);
		if (key.equals("pack_go")) {
			txtTitle.setText("Pack&Go Log");
			strKey = "View: Pack&Go Packages";
		} else if (key.equals("inventory")) {
			txtTitle.setText("Inventory Log");
			strKey = "View:Inventory";
		} else if (key.equals("pick_list")) {
			txtTitle.setText("Pick List Log");
		} else if (key.equals("start_new_pick")) {
			txtTitle.setText("START NEW PICK LIST");
		}

	}

	class ListAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;

		public ListAdapter(Activity activity) {
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 5;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final ViewHolder holder;
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_home_pack_go_listitems, null);
				holder = new ViewHolder();
				holder.layout_items = (LinearLayout) convertView
						.findViewById(R.id.linear_layout_items);
				holder.layout_dialog = (LinearLayout) convertView
						.findViewById(R.id.linear_layout_dialogopen);
				holder.imgOpen = (ImageView) convertView
						.findViewById(R.id.img_open);
				holder.imgShare = (ImageView) convertView
						.findViewById(R.id.img_share);
				holder.relative_popUp = (RelativeLayout) convertView
						.findViewById(R.id.rel_dialog_popup);
				holder.rel_rowItems = (RelativeLayout) convertView
						.findViewById(R.id.rel_items_row);
				holder.imgInvite = (ImageView) convertView
						.findViewById(R.id.img_invite);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.layout_items.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					holder.layout_dialog.setVisibility(View.VISIBLE);
					holder.layout_items.setVisibility(View.GONE);
				}
			});
			holder.imgOpen.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					OpenNewEntryFragment entryFragment = new OpenNewEntryFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					Bundle bundle = new Bundle();
					bundle.putString("Title_Open", strKey);
					entryFragment.setArguments(bundle);
					ft.replace(R.id.container, entryFragment);
					ft.addToBackStack(null);
					ft.commit();
				}
			});

			holder.imgShare.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					holder.relative_popUp.setVisibility(View.VISIBLE);
				}
			});
			holder.imgInvite.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					InviteContactFragment contactFragment = new InviteContactFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					ft.replace(R.id.container, contactFragment);
					ft.addToBackStack(null);
					ft.commit();
				}
			});
			return convertView;
		}
	}

	class ViewHolder {
		private LinearLayout layout_items, layout_dialog;
		private RelativeLayout relative_popUp, rel_rowItems;
		private ImageView imgOpen, imgShare, imgInvite;
	}

}
package com.appalmighty.ezefind.newentry.fgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.act.PickGoActivity;

public class ContainerDetailsFragment extends Fragment {
	String titleKey;
	TextView textViewTitle;
	ImageButton imgPick;
	RelativeLayout rel_options;
	Button backbutton;
	Button btnClose;
	ImageView addToExitingList, newPickList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_container_details,
				container, false);
		titleKey = getArguments().getString("Title_Open");
		textViewTitle = (TextView) rootView.findViewById(R.id.txt_title_header);
		textViewTitle.setText(titleKey);
		imgPick = (ImageButton) rootView.findViewById(R.id.img_picklist);
		backbutton = (Button) rootView.findViewById(R.id.back_button);
		rel_options = (RelativeLayout) rootView
				.findViewById(R.id.layout_selectoptions);

		addListerToPickOption(rootView);

		imgPick.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rel_options.setVisibility(View.VISIBLE);
			}
		});
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		return rootView;
	}

	private void addListerToPickOption(View rootView) {
		btnClose = (Button) rootView.findViewById(R.id.btnclose);
		addToExitingList = (ImageView) rootView
				.findViewById(R.id.id_add_exiting_list);
		newPickList = (ImageView) rootView.findViewById(R.id.new_pick_list);
		addToExitingList = (ImageView) rootView
				.findViewById(R.id.id_add_exiting_list);

		btnClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				rel_options.setVisibility(View.GONE);
			}
		});
		newPickList.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//				ft.setCustomAnimations(R.anim.slide_in_right,
//						R.anim.slide_out_left, R.anim.slide_in_left,
//						R.anim.slide_out_right);
				ft.replace(R.id.container,
						StartPackingFragment.newInstance("pick_list"));
				ft.addToBackStack(null);
				ft.commit();

				// Intent intent1 = new Intent(getActivity(),
				// PickGoActivity.class);
				// intent1.putExtra("Pack_go", "start_new_pick");
				// startActivity(intent1);
				// getActivity().overridePendingTransition(R.anim.fade_in,
				// R.anim.fade_out);
				// getActivity().finish();
			}
		});
		addToExitingList.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// FragmentManager fm = getFragmentManager();
				// FragmentTransaction ft = fm.beginTransaction();
				// ft.setCustomAnimations(R.anim.slide_in_right,
				// R.anim.slide_out_left, R.anim.slide_in_left,
				// R.anim.slide_out_right);
				// ft.replace(R.id.container,
				// PickGoFragment.newInstance("pack_go"));
				// ft.addToBackStack(null);
				// ft.commit();
				Intent intent1 = new Intent(getActivity(), PickGoActivity.class);
				intent1.putExtra("Pack_go", "pick_list");
				startActivity(intent1);
				getActivity().overridePendingTransition(R.anim.fade_in,
						R.anim.fade_out);
				getActivity().finish();
			}
		});
	}

}
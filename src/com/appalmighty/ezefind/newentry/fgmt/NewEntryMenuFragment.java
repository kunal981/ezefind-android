package com.appalmighty.ezefind.newentry.fgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.report.act.ViewReportMenuActivity;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.util.Helper.ToastUi;
import com.appalmighty.ezefind.viewinventory.act.ViewInventoryActivity;

public class NewEntryMenuFragment extends Fragment implements OnClickListener {
	TextView btnBag, btnBox, btnSingleItem, btnBin;

	private ImageView buttonNewEntry, buttonReport, buttonViewInventory,
			buttonSetting;

	private Button buttonPacking, buttonInventory;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_newentry_menu_,
				container, false);
		buttonReport = (ImageView) rootView.findViewById(R.id.id_img_report);
		buttonViewInventory = (ImageView) rootView
				.findViewById(R.id.id_img_view_entry);
		buttonSetting = (ImageView) rootView.findViewById(R.id.id_img_setting);
		btnBag = (TextView) rootView.findViewById(R.id.button_bag);
		btnBox = (TextView) rootView.findViewById(R.id.button_box);
		btnSingleItem = (TextView) rootView
				.findViewById(R.id.button_single_item);
		btnBin = (TextView) rootView.findViewById(R.id.button_bin);
		btnBag.setOnClickListener(this);
		btnBox.setOnClickListener(this);
		btnSingleItem.setOnClickListener(this);
		btnBin.setOnClickListener(this);

		buttonReport.setOnClickListener(this);
		buttonViewInventory.setOnClickListener(this);
		buttonSetting.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		NewEntryFormFragment newEntryFormFragment = null;

		switch (id) {
		case R.id.button_bag:
			newEntryFormFragment = NewEntryFormFragment
					.newInstance(AppConstant.KEY_BAG);
			switchToFragment(newEntryFormFragment);
			break;
		case R.id.button_box:
			newEntryFormFragment = NewEntryFormFragment
					.newInstance(AppConstant.KEY_BOX);
			switchToFragment(newEntryFormFragment);
			break;
		case R.id.button_single_item:
			newEntryFormFragment = NewEntryFormFragment
					.newInstance(AppConstant.KEY_SINGLE_ITEM);
			switchToFragment(newEntryFormFragment);
			break;
		case R.id.button_bin:
			newEntryFormFragment = NewEntryFormFragment
					.newInstance(AppConstant.KEY_BIN);
			switchToFragment(newEntryFormFragment);
			break;

		case R.id.id_img_report:
			Intent intentReport = new Intent(getActivity(),
					ViewReportMenuActivity.class);
			startActivity(intentReport);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_img_view_entry:
			Intent i = new Intent(getActivity(), ViewInventoryActivity.class);
			startActivity(i);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_img_setting:
			ToastUi.print(getActivity(), "Coming Soon");
			break;
		default:
			break;
		}

	}

	public void switchToFragment(Fragment newEntryFormFragment) {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.container, newEntryFormFragment);
		ft.addToBackStack(null);
		ft.commit();
	}
}

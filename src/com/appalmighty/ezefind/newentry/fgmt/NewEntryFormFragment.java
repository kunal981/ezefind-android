package com.appalmighty.ezefind.newentry.fgmt;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.location.MapActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.newentry.act.AddItemsActivity;
import com.appalmighty.ezefind.newentry.act.EditItemsActivity;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;

public class NewEntryFormFragment extends Fragment implements OnClickListener {

	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;

	private static final String IMAGE_DIRECTORY_NAME = "ezeFind";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;

	private static final int ZBAR_SCANNER_REQUEST = 0;

	private TextView textItemNumber, textBarcodeNumber;
	private ImageView imageBox, imageBag, imageSingle, imageBin;
	private ImageView imgBtnCancel, imgBtnSave, imageProfile, imgAddItems,
			imgEditItems;

	private Button buttonScan, buttonOtherBarcode, buttonScanOk,
			buttonScanCancel, buttonSaveProfile, buttonAddProfile,
			buttonAddCateGory, buttonBack;
	private EditText profileName, edtDescription, edtPrice;
	private Button buttonClose, buttonLocationClose;

	private RelativeLayout rLocPlace, rLocShelf, rLocDraw, rLocNear, rLocGPS,
			rLocAddnew;

	private RelativeLayout containerBarcode, containerBarcodeScan,
			containerAddProfile, containerAddCategory, containerCaution,
			containerLocation;

	private Button editBarCode, editBarCode2, editProfile, editCategory,
			editCaution, editLocation;

	public static final String KEY_ARG = "key";

	public String barCode;

	private String title, package_type, imageValue, userid, type, barcodeValue,
			category_id, description, caution, location, location_type,
			platform, price, value;

	private SharedPreferences preferences;
	private EditText edtBag;
	private String plateform;
	private int callingbarcode = 0;

	public static NewEntryFormFragment newInstance(String string) {
		NewEntryFormFragment fragment = new NewEntryFormFragment();
		Bundle bundle = new Bundle();
		bundle.putString(KEY_ARG, string);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(
				R.layout.fragment_new_entry_bag_layout, container, false);
		preferences = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		textItemNumber = (TextView) rootView
				.findViewById(R.id.id_text_item_number);
		imageBox = (ImageView) rootView.findViewById(R.id.id_image_box);
		imageBag = (ImageView) rootView.findViewById(R.id.id_image_bag);
		imageSingle = (ImageView) rootView
				.findViewById(R.id.id_image_single_item);
		imgAddItems = (ImageView) rootView.findViewById(R.id.img_add);
		imgEditItems = (ImageView) rootView.findViewById(R.id.img_edit);
		imageBin = (ImageView) rootView.findViewById(R.id.id_image_bin);
		imgBtnCancel = (ImageView) rootView.findViewById(R.id.img_btn_cancel);
		imgBtnSave = (ImageView) rootView.findViewById(R.id.img_btn_save);
		imgBtnCancel.setOnClickListener(this);
		imgBtnSave.setOnClickListener(this);
		imgAddItems.setOnClickListener(this);
		imgEditItems.setOnClickListener(this);
		editBarCode = (Button) rootView.findViewById(R.id.edt_barcode);
		editBarCode2 = (Button) rootView.findViewById(R.id.edt_barcode_other);
		editProfile = (Button) rootView.findViewById(R.id.edt_member);
		edtDescription = (EditText) rootView.findViewById(R.id.edt_description);
		edtPrice = (EditText) rootView.findViewById(R.id.edt_price);
		edtBag = (EditText) rootView.findViewById(R.id.edt_bag);
		editCategory = (Button) rootView.findViewById(R.id.edt_category);
		editCaution = (Button) rootView.findViewById(R.id.edt_caution);
		editLocation = (Button) rootView.findViewById(R.id.edt_location);
		buttonBack = (Button) rootView.findViewById(R.id.back_button_);
		containerBarcode = (RelativeLayout) rootView
				.findViewById(R.id.container_barcode);
		containerBarcodeScan = (RelativeLayout) rootView
				.findViewById(R.id.container_id_scan);
		containerAddProfile = (RelativeLayout) rootView
				.findViewById(R.id.container_add_profile);
		containerAddCategory = (RelativeLayout) rootView
				.findViewById(R.id.container_add_category);
		containerCaution = (RelativeLayout) rootView
				.findViewById(R.id.container_caution);
		containerLocation = (RelativeLayout) rootView
				.findViewById(R.id.container_location);

		initHeaderItem();

		addClickListerToInputs();

		initBarcodeContainerComp(rootView);
		initProfileContainerComp(rootView);
		initCategoryContainerComp(rootView);
		initCautionContainer(rootView);
		initLocationContainer(rootView);
		buttonBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		return rootView;
	}

	private void initProfileContainerComp(View rootView) {
		buttonSaveProfile = (Button) rootView.findViewById(R.id.btn_save);
		imageProfile = (ImageView) rootView.findViewById(R.id.img_userProfile);
		buttonAddProfile = (Button) rootView.findViewById(R.id.btn_addphoto);
		profileName = (EditText) rootView.findViewById(R.id.edt_entername);
		buttonSaveProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerAddProfile.setVisibility(View.GONE);
			}
		});
		buttonAddProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectImage();
			}
		});

	}

	private void initBarcodeContainerComp(View rootView) {

		buttonScan = (Button) rootView.findViewById(R.id.button_scan);
		buttonScanOk = (Button) rootView.findViewById(R.id.button_scan_ok);
		buttonScanCancel = (Button) rootView.findViewById(R.id.button_cancel);
		textBarcodeNumber = (TextView) rootView
				.findViewById(R.id.text_barcode_number);
		buttonScan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerBarcode.setVisibility(View.GONE);
				containerBarcodeScan.setVisibility(View.VISIBLE);
				buttonScanOk.setText("Scan");
				buttonScanOk.setTag("Scan");
			}
		});
		buttonScanCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerBarcodeScan.setVisibility(View.GONE);
			}
		});
		buttonScanOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (v.getTag().toString().toLowerCase(Locale.getDefault())
						.equals("scan")) {
					launchScanner(v);
				} else if (v.getTag().toString()
						.toLowerCase(Locale.getDefault()).equals("ok")) {
					if (callingbarcode == 1) {
						editBarCode.setText(barCode);
					} else if (callingbarcode == 2) {
						editBarCode2.setText(barCode);
					}
					containerBarcodeScan.setVisibility(View.GONE);
				}

			}
		});

	}

	private void initBarcode2ContainerComp(View rootView) {

		buttonOtherBarcode = (Button) rootView
				.findViewById(R.id.edt_barcode_other);
		buttonOtherBarcode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerBarcode.setVisibility(View.GONE);
				containerBarcodeScan.setVisibility(View.VISIBLE);
			}
		});
		buttonScanOk = (Button) rootView.findViewById(R.id.button_scan_ok);
		buttonScanCancel = (Button) rootView.findViewById(R.id.button_cancel);
		textBarcodeNumber = (TextView) rootView
				.findViewById(R.id.text_barcode_number);
		buttonScanCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerBarcodeScan.setVisibility(View.GONE);
			}
		});
		buttonScanOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (v.getTag().toString().toLowerCase(Locale.getDefault())
						.equals("scan")) {
					launchScanner(v);
				} else if (v.getTag().toString()
						.toLowerCase(Locale.getDefault()).equals("ok")) {
					editBarCode.setText(barCode);
					containerBarcodeScan.setVisibility(View.GONE);
				}

			}
		});

	}

	private void initCategoryContainerComp(View rootView) {

		buttonAddCateGory = (Button) rootView
				.findViewById(R.id.id_button_add_category);
		buttonAddCateGory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// containerBarcode.setVisibility(View.GONE);
				containerAddCategory.setVisibility(View.GONE);
				addOtherCategoryWindow();
			}
		});

	}

	private void initCautionContainer(View rootView) {

		buttonClose = (Button) rootView.findViewById(R.id.back_close);
		buttonClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerCaution.setVisibility(View.GONE);
			}
		});

	}

	private void initLocationContainer(View rootView) {

		buttonLocationClose = (Button) rootView
				.findViewById(R.id.location_close);
		rLocPlace = (RelativeLayout) (rootView).findViewById(R.id.id_rel_place);
		rLocShelf = (RelativeLayout) (rootView).findViewById(R.id.id_rel_shelf);
		rLocDraw = (RelativeLayout) (rootView).findViewById(R.id.id_rel_draw);
		rLocNear = (RelativeLayout) (rootView).findViewById(R.id.id_rel_near);
		rLocGPS = (RelativeLayout) (rootView).findViewById(R.id.id_rel_gps);
		rLocAddnew = (RelativeLayout) (rootView)
				.findViewById(R.id.id_rel_add_new);

		rLocPlace.setOnClickListener(this);
		rLocShelf.setOnClickListener(this);
		rLocDraw.setOnClickListener(this);
		rLocNear.setOnClickListener(this);
		rLocGPS.setOnClickListener(this);
		rLocAddnew.setOnClickListener(this);

		buttonLocationClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerLocation.setVisibility(View.GONE);
			}
		});

	}

	protected void addOtherCategoryWindow() {
		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_add_category, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		Button btnDismiss = (Button) popupView
				.findViewById(R.id.id_button_save);
		btnDismiss.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
				containerAddCategory.setVisibility(View.GONE);
			}
		});

		popupWindow.showAtLocation(containerAddCategory, Gravity.CENTER, 0, 0);

	}

	private void addLocationPopuWindow(String string) {
		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.popup_layout_draw_,
				null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		Button btnDismiss = (Button) popupView.findViewById(R.id.back_button);
		Button btnSave = (Button) popupView.findViewById(R.id.btn_save);
		TextView textHeader = (TextView) popupView.findViewById(R.id.txt_title);
		textHeader.setText(string);
		btnDismiss.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
				containerLocation.setVisibility(View.GONE);
			}
		});
		btnSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
				containerLocation.setVisibility(View.GONE);
			}
		});

		popupWindow.showAtLocation(containerLocation, Gravity.CENTER, 0, 0);
	}

	private void addClickListerToInputs() {
		editBarCode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				callingbarcode = 1;
				containerBarcode.setVisibility(View.VISIBLE);

			}
		});
		editBarCode2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				callingbarcode = 2;
				containerBarcode.setVisibility(View.VISIBLE);

			}
		});
		editProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerAddProfile.setVisibility(View.VISIBLE);

			}
		});
		editCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerAddCategory.setVisibility(View.VISIBLE);

			}
		});
		editCaution.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerCaution.setVisibility(View.VISIBLE);

			}
		});
		editLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerLocation.setVisibility(View.VISIBLE);

			}
		});

	}

	private void initHeaderItem() {

		String key = getArguments().getString(KEY_ARG);

		switch (key) {
		case AppConstant.KEY_BAG:
			textItemNumber.setText("Bag# 01");
			imageBag.setVisibility(View.VISIBLE);
			break;
		case AppConstant.KEY_BOX:
			textItemNumber.setText("Box# 01");
			imageBox.setVisibility(View.VISIBLE);

			break;
		case AppConstant.KEY_SINGLE_ITEM:
			textItemNumber.setText("Item# 01");
			imageSingle.setVisibility(View.VISIBLE);
			break;
		case AppConstant.KEY_BIN:
			textItemNumber.setText("Bin# 01");
			imageBin.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}

	}

	public void launchScanner(View v) {
		if (isCameraAvailable()) {
			Intent intent = new Intent(getActivity(), ZBarScannerActivity.class);
			startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
		} else {
			Toast.makeText(getActivity(), "Rear Facing Camera Unavailable",
					Toast.LENGTH_SHORT).show();
		}
	}

	public boolean isCameraAvailable() {
		PackageManager pm = getActivity().getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	protected void selectImage() {

		final CharSequence[] options = { "Take Photo", "Choose from Gallery",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from Gallery")) {
					chooseFromGallery();

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	protected void chooseFromGallery() {

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	protected void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHOCO_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHOCO_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 2;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		Log.d("Insample size", inSampleSize + "");
		return inSampleSize;
	}

	private void previewCapturedImage() {
		// TODO Auto-generated method stub
		// bimatp factory
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inJustDecodeBounds = true;

		// downsizing image as it throws OutOfMemory Exception for larger
		// images
		BitmapFactory.decodeFile(fileUri.getPath(), options);

		// options.inSampleSize = 8;

		int imageHeight = options.outHeight;
		int imageWidth = options.outWidth;
		String imageType = options.outMimeType;

		Log.d("Height: ", imageHeight + "");
		Log.d("width: ", imageWidth + "");
		Log.d("imageType: ", imageType + "");

		options.inSampleSize = calculateInSampleSize(options, 250, 250);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		Bitmap bitMap = BitmapFactory.decodeFile(fileUri.getPath(), options);

		Log.d("Tag ", "Image dimension after sampling");
		int imageHeight_a = options.outHeight;
		int imageWidth_1 = options.outWidth;
		Log.d("Height: ", imageHeight_a + "");
		Log.d("width: ", imageWidth_1 + "");

		imageProfile.setImageBitmap(bitMap);
		deleteRecursive(mediaStorageDir);

	}

	private void previewGalleryImage(Intent data) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// downsizing image as it throws OutOfMemory Exception for larger
		// images
		// options.inSampleSize = 8;
		Uri selectedImage = data.getData();
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		if (getActivity().getContentResolver() != null) {
			Cursor cursor = getActivity().getContentResolver().query(
					selectedImage, filePathColumn, null, null, null);

			if (cursor != null) {
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String filePath = cursor.getString(columnIndex);
				cursor.close();

				BitmapFactory.decodeFile(filePath, options);
				// bitmap = MediaStore.Images.Media.getBitmap(
				// this.getContentResolver(), data.getData());

				options.inSampleSize = calculateInSampleSize(options, 250, 250);

				// Decode bitmap with inSampleSize set
				options.inJustDecodeBounds = false;

				Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

				Log.d("Tag ", "Image dimension after sampling");
				int imageHeight_a = options.outHeight;
				int imageWidth_1 = options.outWidth;
				Log.d("Height: ", imageHeight_a + "");
				Log.d("width: ", imageWidth_1 + "");

				// bitmap = getResizedBitmap(bitmap, frameHeight, frameWidth);
				// // bitmap = getResizedBitmap(bitmap, 500, 500);
				//
				imageProfile.setImageBitmap(bitmap);
				deleteRecursive(mediaStorageDir);
			} else {
				Toast.makeText(getActivity(),
						"Unable to locate image .. Please try again",
						Toast.LENGTH_LONG).show();
			}

		} else {
			Toast.makeText(getActivity(),
					"Unable to locate image .. Please try again",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				deleteRecursive(child);
			}
		}

		boolean status = fileOrDirectory.delete();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case ZBAR_SCANNER_REQUEST:
			if (resultCode == Activity.RESULT_OK) {
				// Toast.makeText(
				// getActivity(),
				// "Scan Result = "
				// + data.getStringExtra(ZBarConstants.SCAN_RESULT),
				// Toast.LENGTH_SHORT).show();
				if (data != null) {
					barCode = data.getStringExtra(ZBarConstants.SCAN_RESULT);
					textBarcodeNumber.setText(barCode);
					buttonScanOk.setText("Ok");
					buttonScanOk.setTag("Ok");
				}

			} else if (resultCode == Activity.RESULT_CANCELED && data != null) {
				String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
				if (!TextUtils.isEmpty(error)) {
					Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT)
							.show();
				}
			}
			break;
		case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getActivity(), "User cancelled image capture",
						Toast.LENGTH_SHORT).show();
			} else {
				// failed to capture image
				Toast.makeText(getActivity(), "Sorry! Failed to capture image",
						Toast.LENGTH_SHORT).show();
			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// video successfully recorded
				// preview the recorded video

				previewGalleryImage(data);

			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(getActivity(),
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(getActivity(), "Sorry! Process Failed",
						Toast.LENGTH_SHORT).show();
			}
			break;
		}

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.id_rel_place:
			addLocationPopuWindow("Place");

			break;
		case R.id.id_rel_shelf:
			addLocationPopuWindow("Shelf");

			break;
		case R.id.id_rel_draw:
			addLocationPopuWindow("Draw");

			break;
		case R.id.id_rel_near:
			addLocationPopuWindow("Near");

			break;
		case R.id.id_rel_gps:
			Intent intent = new Intent(getActivity(), MapActivity.class);
			startActivity(intent);
			break;
		case R.id.id_rel_add_new:
			addLocationPopuWindow("Add new");

			break;
		case R.id.img_btn_cancel:
			showAlertDialog(getActivity(), "Cancel", "Are you sure to cancel",
					false);
			// getActivity().onBackPressed();
			break;
		case R.id.img_btn_save:

			saveNewEntry();

			// getActivity().onBackPressed();

			break;
		case R.id.img_add:
			Intent intent_additems = new Intent(getActivity(),
					AddItemsActivity.class);
			startActivity(intent_additems);
			break;
		case R.id.img_edit:
			Intent intent_edit = new Intent(getActivity(),
					EditItemsActivity.class);
			startActivity(intent_edit);
			break;
		default:
			break;
		}

	}

	private void saveNewEntry() {

		List<NameValuePair> parameter = null;
		title = edtBag.getText().toString();
		package_type = preferences.getString(AppConstant.PICK_GO_TYPE,
				"inventory");
		userid = preferences.getString(AppConstant.KEY_USER_ID, "2");
		type = getArguments().getString(KEY_ARG);
		barCode = "12431243";
		// barCode = editBarCode.getText().toString();
		category_id = "2";// static no information
		description = "test desc";
		// description = edtDescription.getText().toString();
		caution = "caution";
		// caution = editCaution.getText().toString();
		location = "location";
		location_type = "location_type";
		price = "20";
		plateform = "android";
		// price = edtPrice.getText().toString();

		if (validateForm()) {
			parameter = new LinkedList<NameValuePair>();
			parameter
					.add(new BasicNameValuePair("title", String.valueOf(title)));
			parameter.add(new BasicNameValuePair("package_type", String
					.valueOf(package_type)));
			parameter.add(new BasicNameValuePair("userid", String
					.valueOf(userid)));
			parameter.add(new BasicNameValuePair("type", String.valueOf(type)));
			parameter.add(new BasicNameValuePair("barcode", String
					.valueOf(barCode)));
			parameter.add(new BasicNameValuePair("category_id", String
					.valueOf(category_id)));
			parameter.add(new BasicNameValuePair("description", String
					.valueOf(description)));
			parameter.add(new BasicNameValuePair("caution", String
					.valueOf(caution)));
			parameter.add(new BasicNameValuePair("location", location));
			parameter
					.add(new BasicNameValuePair("location_type", location_type));
			parameter.add(new BasicNameValuePair("price", price));
			parameter.add(new BasicNameValuePair("plateform", String
					.valueOf("android")));

			new AsyncSaveUserFormTask(parameter).execute();
		}
	}

	private boolean validateForm() {

		if (title.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Title",
					"Please enter title value", true);
			return false;
		}

		return true;
	}

	public class AsyncSaveUserFormTask extends AsyncTask<String, Void, String> {

		List<NameValuePair> parameter = null;

		public AsyncSaveUserFormTask(List<NameValuePair> parameter) {
			// TODO Auto-generated constructor stubthis
			this.parameter = parameter;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.has("status")) {

					if (jObj.getInt("status") == 1) {
						ViewUtil.hideProgressDialog();
						Toast.makeText(getActivity(),
								"Package Added sucessfully", Toast.LENGTH_SHORT)
								.show();

					} else {
						Log.e("Response", result);
						ViewUtil.hideProgressDialog();
					}
				} else {

					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			String result = NetworkConnector.saveUserForm(parameter);
			return result;
		}

	}

	public static void showAlertDialog(final Activity context, String title,
			String message, boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								context.onBackPressed();
								dialog.cancel();

							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}

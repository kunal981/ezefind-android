package com.appalmighty.ezefind.newentry.fgmt;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.viewinventory.fgmt.OnStartPickListFragment;

public class StartPackingFragment extends Fragment {
	private Button btnStart;
	private TextView txtTitle;
	private EditText startDate;
	private EditText startTime;

	static final int DATE_DIALOG_ID = 999;

	public static final String TAG = StartPackingFragment.class.getName();
	public static final String KEY = "key";
	String key;

	public static StartPackingFragment newInstance(String key) {
		StartPackingFragment fragment = new StartPackingFragment();
		Bundle bundle = new Bundle();
		bundle.putString(KEY, key);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_start_packing,
				container, false);
		txtTitle = (TextView) rootView.findViewById(R.id.txt_title_);

		btnStart = (Button) rootView.findViewById(R.id.btn_start);
		startDate = (EditText) rootView.findViewById(R.id.edt_start_date);
		startTime = (EditText) rootView.findViewById(R.id.edt_start_timer);
		intHeaderTitle();

		setTimer();

		addClickListener();
		return rootView;
	}

	private void addClickListener() {

		btnStart.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				key = getArguments().getString(KEY);
				if (key.equals("pack_go")) {
					NewEntryMenuFragment fragment = new NewEntryMenuFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					ft.replace(R.id.container, fragment);
					ft.addToBackStack(null);
					ft.commit();
				} else if (key.equals("inventory")) {
					NewEntryMenuFragment fragment = new NewEntryMenuFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					ft.replace(R.id.container, fragment);
					ft.addToBackStack(null);
					ft.commit();
				} else if (key.equals("pick_list")) {
					OnStartPickListFragment fragment = new OnStartPickListFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					ft.replace(R.id.container, fragment);
					ft.addToBackStack(null);
					ft.commit();
				}

			}
		});

		startDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment picker = new DatePickerFragment();
				picker.show(getFragmentManager(), "datePicker");
			}

		});
		startTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TimePickerFragment picker = new TimePickerFragment();
				picker.show(getFragmentManager(), "timePicker");
			}

		});

	}

	private void setTimer() {

		Date today = Calendar.getInstance().getTime();

		setDate(today);
		setTime(today);

	}

	private void setDate(Date today) {
		// Create an instance of SimpleDateFormat used for formatting
		// the string representation of date (month/day/year)
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());

		// Get the date today using Calendar object.
		// Using DateFormat format method we can create a string
		// representation of a date with the defined format.
		String reportDate = df.format(today);
		startDate.setText(reportDate);

	}

	private void setTime(Date today) {

		DateFormat timeFormat = new SimpleDateFormat("HH:mm a",
				Locale.getDefault());

		Log.e("Formater", timeFormat.format(today));
		startTime.setText(timeFormat.format(today));

	}

	private void intHeaderTitle() {
		// TODO Auto-generated method stub
		String key = getArguments().getString(KEY);
		if (key.equals("pack_go")) {
			txtTitle.setText("START PACKING");
		} else if (key.equals("inventory")) {
			txtTitle.setText("START INVENTORY");
		} else if (key.equals("pick_list")) {
			txtTitle.setText("START NEW PICK LIST");
		}
	}

	public class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			Calendar c = Calendar.getInstance();
			c.set(year, month, day);
			Date date = c.getTime();
			setDate(date);
		}
	}

	public class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of DatePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					false);
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			c.set(year, month, day, hourOfDay, minute);
			setTime(c.getTime());
		}
	}
}

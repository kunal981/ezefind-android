package com.appalmighty.ezefind.newentry.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.appalmighty.ezefind.R;

public class InviteContactFragment extends Fragment {
	private ListAdapter1 adapter;
	private ListView listview;
	private Button btnInvite;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_invite_page,
				container, false);
		listview = (ListView) rootView.findViewById(R.id.listView_contact);
		adapter = new ListAdapter1(getActivity());
		listview.setAdapter(adapter);
		btnInvite = (Button) rootView.findViewById(R.id.btn_invite);
		btnInvite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InvitationVerficationFragment inviteContactFragment = new InvitationVerficationFragment();
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				ft.replace(R.id.container, inviteContactFragment);
				ft.addToBackStack(null);
				ft.commit();
			}
		});

		return rootView;
	}

	class ListAdapter1 extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;

		public ListAdapter1(Activity activity) {
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 9;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.fragment_invite_page_listitems, null);

			}
			return convertView;
		}

	}
}

package com.appalmighty.ezefind.newentry.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.R;

public class OpenContainerFragment extends Fragment {
	private GridviewAdapter1 adapter;
	private GridView gridView;
	Button backbutton;
	ImageView btnDone, imgNewEntry;
	ImageButton imageButton_filter;
	RelativeLayout layout_filter;
	String title;
	TextView textViewTitle;

	public static OpenContainerFragment newInstance() {
		OpenContainerFragment fragment = new OpenContainerFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_open_container,
				container, false);
		title = getArguments().getString("Title_Open");
		gridView = (GridView) rootView
				.findViewById(R.id.id_gridview_open_container);
		backbutton = (Button) rootView.findViewById(R.id.back_button);
		imageButton_filter = (ImageButton) rootView
				.findViewById(R.id.img_filter);
		btnDone = (ImageView) rootView.findViewById(R.id.btnDone);
		imgNewEntry = (ImageView) rootView.findViewById(R.id.id_new_entry);
		layout_filter = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter);
		textViewTitle = (TextView) rootView.findViewById(R.id.txt_title_header);
		textViewTitle.setText(title);
		adapter = new GridviewAdapter1(getActivity());
		gridView.setAdapter(adapter);
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		imageButton_filter.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout_filter.setVisibility(View.VISIBLE);
			}
		});

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				// Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT)
				// .show();
				Log.e("clicked", "Hello");
			}
		});
		btnDone.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout_filter.setVisibility(View.GONE);

			}
		});
		imgNewEntry.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout_filter.setVisibility(View.GONE);

			}
		});
		return rootView;
	}

	class GridviewAdapter1 extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;

		public GridviewAdapter1(Activity activity) {
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 8;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.fragment_open_container_griditems, null);
				ImageView imageView = (ImageView) convertView
						.findViewById(R.id.img_clothes);
				imageView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", title);
						openContainerFragment.setArguments(bundle);
						ft.replace(R.id.container, openContainerFragment);
						ft.addToBackStack(null);
						ft.commit();

					}
				});

			}
			return convertView;
		}

	}

}

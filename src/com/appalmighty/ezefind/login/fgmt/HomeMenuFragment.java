package com.appalmighty.ezefind.login.fgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.act.PickGoActivity;
import com.appalmighty.ezefind.report.act.ViewReportActivity;
import com.appalmighty.ezefind.report.act.ViewReportMenuActivity;
import com.appalmighty.ezefind.util.Helper.ToastUi;
import com.appalmighty.ezefind.viewinventory.act.ViewInventoryActivity;

public class HomeMenuFragment extends Fragment implements OnClickListener {

	public static final String TAG = HomeMenuFragment.class.getName();

	private ImageView buttonNewEntry, buttonReport, buttonViewInventory,
			buttonSetting;

	private Button buttonPacking, buttonInventory;
	private LinearLayout containerMenu;
	private RelativeLayout containerClick, menuCotainer;

	public HomeMenuFragment() {
	}

	public static HomeMenuFragment newInstance() {
		HomeMenuFragment fragment = new HomeMenuFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login_menu,
				container, false);
		buttonNewEntry = (ImageView) rootView
				.findViewById(R.id.id_img_new_entry);
		buttonReport = (ImageView) rootView.findViewById(R.id.id_img_report);
		buttonViewInventory = (ImageView) rootView
				.findViewById(R.id.id_img_inventory);
		buttonSetting = (ImageView) rootView.findViewById(R.id.id_img_setting);
		containerMenu = (LinearLayout) rootView.findViewById(R.id.rel_menu_bar);
		buttonPacking = (Button) rootView.findViewById(R.id.id_buton_pack);
		buttonInventory = (Button) rootView
				.findViewById(R.id.id_buton_inventory);
		containerClick = (RelativeLayout) rootView
				.findViewById(R.id.r_outer_click_container);
		menuCotainer = (RelativeLayout) rootView
				.findViewById(R.id.r_menu_container);
		buttonNewEntry.setOnClickListener(this);
		buttonReport.setOnClickListener(this);
		buttonViewInventory.setOnClickListener(this);
		buttonSetting.setOnClickListener(this);
		containerClick.setOnClickListener(this);
		buttonPacking.setOnClickListener(this);
		buttonInventory.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.id_img_new_entry:
			containerMenu.setVisibility(View.VISIBLE);
			break;
		case R.id.id_img_report:
			Intent intentReport = new Intent(getActivity(),
					ViewReportMenuActivity.class);
			startActivity(intentReport);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_img_inventory:
			Intent i = new Intent(getActivity(), ViewInventoryActivity.class);
			startActivity(i);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_img_setting:
			ToastUi.print(getActivity(), "Coming Soon");
			break;
		case R.id.r_outer_click_container:
			if (containerMenu.isShown()) {
				containerMenu.setVisibility(View.INVISIBLE);
			}
			break;
		case R.id.id_buton_inventory:
			Intent intent1 = new Intent(getActivity(), PickGoActivity.class);
			intent1.putExtra("Pack_go", "inventory");
			startActivity(intent1);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_buton_pack:
			Intent intent = new Intent(getActivity(), PickGoActivity.class);
			intent.putExtra("Pack_go", "pack_go");
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;

		default:
			break;
		}

	}

}
package com.appalmighty.ezefind.login.fgmt;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.login.fgmt.RegisterFragment.AsyncRegistrationTask;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;

public class PasswordFragment extends Fragment {

	public static final String TAG = PasswordFragment.class.getName();
	private EditText edtEmail;
	private Button submitButton;

	private String email;

	public PasswordFragment() {
	}

	public static PasswordFragment newInstance() {
		PasswordFragment fragment = new PasswordFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_reset_password,
				container, false);

		edtEmail = (EditText) rootView.findViewById(R.id.id_email);
		submitButton = (Button) rootView.findViewById(R.id.button_submit);

		submitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				email = edtEmail.getText().toString();
				if (!isEmailEmpty()) {
					new AsyncForgotPasswrodTask().execute(email);
				} else {
					ViewUtil.showAlertDialog(getActivity(), "email",
							"Please enter your email", true);
				}

			}
		});

		return rootView;
	}

	public class AsyncForgotPasswrodTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.has("status"))
					if (jObj.getInt("status") == 1) {
						ViewUtil.hideProgressDialog();
						Toast.makeText(getActivity(),
								"Email sent to registered moblie",
								Toast.LENGTH_LONG).show();

					} else {
						Toast.makeText(getActivity(), "Email not registered",
								Toast.LENGTH_LONG).show();

						Log.e("Response", result);
						ViewUtil.hideProgressDialog();
					}
			} catch (JSONException e) {
				e.printStackTrace();
				ViewUtil.hideProgressDialog();
			}
		}

		public void startLoginActivity() {

			getActivity().onBackPressed();
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("email", String
					.valueOf(params[0])));
			String result = NetworkConnector.forgotPassword(parameter);
			return result;
		}

	}

	protected boolean isEmailEmpty() {
		if (edtEmail.getText().toString().equals("")) {
			return true;
		}
		return false;
	}
}
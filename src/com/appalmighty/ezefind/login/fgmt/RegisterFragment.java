package com.appalmighty.ezefind.login.fgmt;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.dm.zbar.android.scanner.ZBarConstants;

public class RegisterFragment extends Fragment {

	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;

	private static final String IMAGE_DIRECTORY_NAME = "ezeFind";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;

	public static final String TAG = RegisterFragment.class.getName();
	private ImageView imageAddPic;
	private EditText username;
	private EditText password;
	private EditText email;
	private EditText confirmPassword;
	private Button buttonSubmit;

	String userNameString, passwordString, emailString, confirmPasswordString;

	public RegisterFragment() {
	}

	public static RegisterFragment newInstance() {
		RegisterFragment fragment = new RegisterFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_register, container,
				false);

		username = (EditText) rootView.findViewById(R.id.id_username);
		email = (EditText) rootView.findViewById(R.id.id_email);
		password = (EditText) rootView.findViewById(R.id.id_password);
		confirmPassword = (EditText) rootView
				.findViewById(R.id.id_confirm_password);

		imageAddPic = (ImageView) rootView.findViewById(R.id.image_pic);
		buttonSubmit = (Button) rootView.findViewById(R.id.button_submit);

		imageAddPic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectImage();
			}
		});
		buttonSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				userNameString = username.getText().toString();
				passwordString = password.getText().toString();
				emailString = email.getText().toString();
				confirmPasswordString = confirmPassword.getText().toString();
				registerProcess();

			}
		});
		return rootView;
	}

	protected void registerProcess() {

		if (validateUser()) {
			new AsyncRegistrationTask().execute(userNameString, passwordString,
					emailString);
		}

	}

	private boolean validateUser() {

		if (isEmptyEmail()) {
			ViewUtil.showAlertDialog(getActivity(), "Email",
					"Please enter your email", true);
			return false;
		}
		// if (isNotValidEmail()) {
		// ViewUtil.showAlertDialog(getActivity(), "Email",
		// "Please enter your valid email", true);
		// return false;
		// }
		if (isUserNameEmpty()) {
			ViewUtil.showAlertDialog(getActivity(), "User name",
					"Please enter your user name", true);
			return false;
		}
		if (isPasswordEmpty()) {
			ViewUtil.showAlertDialog(getActivity(), "Password",
					"Please enter your password", true);
			return false;
		}
		if (isConfirmPasswordEmpty()) {
			ViewUtil.showAlertDialog(getActivity(), "Confirm Password",
					"Please enter your confirm password ", true);
			return false;
		}
		if (!isEqualPassword()) {
			ViewUtil.showAlertDialog(getActivity(), "Passwrod",
					"Password not matches", true);
			return false;
		}

		return true;
	}

	private boolean isEqualPassword() {
		if (confirmPasswordString.equals(passwordString)) {
			return true;
		}
		return false;
	}

	private boolean isConfirmPasswordEmpty() {
		if (confirmPasswordString.trim().equals("")) {
			return true;
		}
		return false;
	}

	private boolean isPasswordEmpty() {
		if (passwordString.trim().equals("")) {
			return true;
		}

		return false;
	}

	private boolean isUserNameEmpty() {
		if (userNameString.trim().equals("")) {
			return true;
		}
		return false;
	}

	private boolean isNotValidEmail() {
		if (checkValidEmail(emailString)) {
			return true;
		}
		return false;
	}

	/*
	 * An Email validation function which checks the entered email is a valid
	 * email or not. returns true if valid , false if invalid
	 */
	protected boolean checkValidEmail(String email) {
		boolean isValid = false;
		// String PATTERN =
		// "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(email);
		isValid = matcher.matches();
		return isValid;
	}

	private boolean isEmptyEmail() {
		if (emailString.trim().equals("")) {
			return true;
		}
		return false;
	}

	public class AsyncRegistrationTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {
					ViewUtil.hideProgressDialog();
					Toast.makeText(getActivity(), "Successfully Register User",
							Toast.LENGTH_SHORT).show();
					startLoginActivity();

				} else {
					Log.e("Response", result);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		public void startLoginActivity() {

			getActivity().onBackPressed();
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("username", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("password", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("email", String
					.valueOf(params[2])));
			String result = NetworkConnector.registerUser(parameter);
			return result;
		}

	}

	protected void selectImage() {

		final CharSequence[] options = { "Take Photo", "Choose from Gallery",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from Gallery")) {
					chooseFromGallery();

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	protected void chooseFromGallery() {

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getActivity(), "User cancelled image capture",
						Toast.LENGTH_SHORT).show();
			} else {
				// failed to capture image
				Toast.makeText(getActivity(), "Sorry! Failed to capture image",
						Toast.LENGTH_SHORT).show();
			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// video successfully recorded
				// preview the recorded video

				previewGalleryImage(data);

			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(getActivity(),
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(getActivity(), "Sorry! Process Failed",
						Toast.LENGTH_SHORT).show();
			}
			break;
		}

	}

	private void previewCapturedImage() {
		// TODO Auto-generated method stub
		// bimatp factory
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inJustDecodeBounds = true;

		// downsizing image as it throws OutOfMemory Exception for larger
		// images
		BitmapFactory.decodeFile(fileUri.getPath(), options);

		// options.inSampleSize = 8;

		int imageHeight = options.outHeight;
		int imageWidth = options.outWidth;
		String imageType = options.outMimeType;

		options.inSampleSize = calculateInSampleSize(options, 100, 100);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		Bitmap bitMap = BitmapFactory.decodeFile(fileUri.getPath(), options);

		int imageHeight_a = options.outHeight;
		int imageWidth_1 = options.outWidth;

		imageAddPic.setImageBitmap(bitMap);
		deleteRecursive(mediaStorageDir);

	}

	private void previewGalleryImage(Intent data) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// downsizing image as it throws OutOfMemory Exception for larger
		// images
		// options.inSampleSize = 8;
		Uri selectedImage = data.getData();
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		if (getActivity().getContentResolver() != null) {
			Cursor cursor = getActivity().getContentResolver().query(
					selectedImage, filePathColumn, null, null, null);

			if (cursor != null) {
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String filePath = cursor.getString(columnIndex);
				cursor.close();

				BitmapFactory.decodeFile(filePath, options);
				// bitmap = MediaStore.Images.Media.getBitmap(
				// this.getContentResolver(), data.getData());

				options.inSampleSize = calculateInSampleSize(options, 250, 250);

				// Decode bitmap with inSampleSize set
				options.inJustDecodeBounds = false;

				Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

				int imageHeight_a = options.outHeight;
				int imageWidth_1 = options.outWidth;

				// bitmap = getResizedBitmap(bitmap, frameHeight, frameWidth);
				// // bitmap = getResizedBitmap(bitmap, 500, 500);
				//
				imageAddPic.setImageBitmap(bitmap);
				deleteRecursive(mediaStorageDir);
			} else {
				Toast.makeText(getActivity(),
						"Unable to locate image .. Please try again",
						Toast.LENGTH_LONG).show();
			}

		} else {
			Toast.makeText(getActivity(),
					"Unable to locate image .. Please try again",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				deleteRecursive(child);
			}
		}

		boolean status = fileOrDirectory.delete();
	}

	protected void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHOCO_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHOCO_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 2;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		Log.d("Insample size", inSampleSize + "");
		return inSampleSize;
	}

}
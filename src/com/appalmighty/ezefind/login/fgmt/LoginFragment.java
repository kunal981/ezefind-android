package com.appalmighty.ezefind.login.fgmt;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;

public class LoginFragment extends Fragment implements OnClickListener {

	public static final String TAG = LoginFragment.class.getName();

	private TextView textForgotButton;
	private TextView textRegisterButton;
	private Button buttonSubmit;
	private EditText username;
	private String userNameString;
	private EditText password;
	private String passwordString;

	SharedPreferences sharedpreferences;

	String userId;

	private CheckBox rememberMe;

	public LoginFragment() {
	}

	public static LoginFragment newInstance() {
		LoginFragment fragment = new LoginFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login, container,
				false);

		sharedpreferences = getActivity().getSharedPreferences(
				AppConstant.KEY_APP, Context.MODE_PRIVATE);
		textForgotButton = (TextView) rootView
				.findViewById(R.id.id_forgot_password);
		username = (EditText) rootView.findViewById(R.id.id_username);
		rememberMe = (CheckBox) rootView.findViewById(R.id.check_remember_me);
		password = (EditText) rootView.findViewById(R.id.id_password);
		textForgotButton.setOnClickListener(this);
		textRegisterButton = (TextView) rootView.findViewById(R.id.id_register);
		textRegisterButton.setOnClickListener(this);
		buttonSubmit = (Button) rootView.findViewById(R.id.button_submit);
		buttonSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Intent intent = new Intent(getActivity(),
				// MainActivity.class);
				// startActivity(intent);
				// getActivity().overridePendingTransition(R.anim.fade_in,
				// R.anim.fade_out);
				// getActivity().finish();
				//
				userNameString = username.getText().toString();
				passwordString = password.getText().toString();
				if (validateLogin()) {
					new AsyncLoginTask()
							.execute(userNameString, passwordString);
				}

			}
		});
		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		Fragment fragment = null;
		switch (id) {
		case R.id.id_forgot_password:
			fragment = PasswordFragment.newInstance();
			break;
		case R.id.id_register:
			fragment = RegisterFragment.newInstance();
			break;

		default:
			break;
		}

		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.container_login, fragment);
		ft.addToBackStack(null);
		ft.commit();

	}

	protected boolean validateLogin() {
		if (userNameString.trim().equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Email",
					"Please enter your email", true);

			return false;
		}
		if (passwordString.trim().equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Password",
					"Please enter your password", true);
			return false;
		}

		return true;
	}

	/*
	 * An Email validation function which checks the entered email is a valid
	 * email or not. returns true if valid , false if invalid
	 */
	protected boolean checkValidEmail(String email) {
		boolean isValid = false;
		// String PATTERN =
		// "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(email);
		isValid = matcher.matches();
		return isValid;
	}

	/*
	 * A user Login API is called.
	 */

	public class AsyncLoginTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {
					ViewUtil.hideProgressDialog();
					Log.e("userId", "" + jObj.getInt("userid"));
					userId = String.valueOf(jObj.getInt("userid"));
					rememberUser();

					startMainActivity();

				} else {
					Log.e("Response", result);
					ViewUtil.hideProgressDialog();
					ViewUtil.showAlertDialog(getActivity(), "Login Failed",
							"Username and password not match please try again",
							true);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		private void rememberUser() {

			if (rememberMe.isChecked()) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.KEY_USER_NAME, userNameString);
				editor.putString(AppConstant.KEY_USER_PASSWORD, passwordString);
				editor.commit();
			}

		}

		public void startMainActivity() {
			Editor editor = sharedpreferences.edit();
			editor.putString(AppConstant.KEY_USER_ID, userId);
			editor.commit();

			Intent intent = new Intent(getActivity(), MainActivity.class);
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			getActivity().finish();
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("username", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("password", String
					.valueOf(params[1])));
			String result = NetworkConnector.loginAuthentication(parameter);
			return result;
		}

	}

}
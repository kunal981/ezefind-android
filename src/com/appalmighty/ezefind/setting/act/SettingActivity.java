package com.appalmighty.ezefind.setting.act;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.report.fgmt.ReportAllFragment;
import com.appalmighty.ezefind.report.fgmt.ReportCategoryFragment;
import com.appalmighty.ezefind.report.fgmt.ReportDateFragment;
import com.appalmighty.ezefind.report.fgmt.ReportItemFragment;
import com.appalmighty.ezefind.report.fgmt.ReportLocationFragment;
import com.appalmighty.ezefind.report.fgmt.ReportMemberFragment;
import com.appalmighty.ezefind.report.fgmt.ReportValueFragment;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentAccount;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentEntry;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentNetwork;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentProfile;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentSaveEntry;

public class SettingActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_activity_setting);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, SettingFragment.newInstance())
					.commit();
		}

	}

	public static class SettingFragment extends Fragment implements
			OnClickListener {

		ImageView imageButtonProfile, imageButtonAcc, imageButtonEntry,
				imageButtonNetwork, imageButtonSaveSetting;

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_layout_setting_main, container, false);
			imageButtonProfile = (ImageView) rootView
					.findViewById(R.id.id_btn_profile);
			imageButtonAcc = (ImageView) rootView.findViewById(R.id.id_btn_acc);
			imageButtonEntry = (ImageView) rootView
					.findViewById(R.id.id_btn_es);
			imageButtonNetwork = (ImageView) rootView
					.findViewById(R.id.id_btn_ns);
			imageButtonSaveSetting = (ImageView) rootView
					.findViewById(R.id.id_btn_ss);
			imageButtonProfile.setOnClickListener(this);
			imageButtonAcc.setOnClickListener(this);
			imageButtonEntry.setOnClickListener(this);
			imageButtonNetwork.setOnClickListener(this);
			imageButtonSaveSetting.setOnClickListener(this);

			return rootView;
		}

		public static Fragment newInstance() {
			SettingFragment fragment = new SettingFragment();
			return fragment;
		}

		@Override
		public void onClick(View v) {
			int id = v.getId();
			Fragment fragment = null;
			switch (id) {
			case R.id.id_btn_profile:
				fragment = SettingFragmentProfile.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.id_btn_acc:
				fragment = SettingFragmentAccount.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.id_btn_es:
				fragment = SettingFragmentEntry.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.id_btn_ns:
				fragment = SettingFragmentNetwork.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.id_btn_ss:
				fragment = SettingFragmentSaveEntry.newInstance();
				switchToFragment(fragment);
				break;

			default:
				break;
			}

		}

		private void switchToFragment(Fragment fragment) {

			FragmentManager fm = getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right,
					R.anim.slide_out_left, R.anim.slide_in_left,
					R.anim.slide_out_right);
			ft.replace(R.id.container, fragment);
			ft.addToBackStack(null);
			ft.commit();

		}
	}

}

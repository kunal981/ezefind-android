package com.appalmighty.ezefind.setting.fgmt;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.appalmighty.ezefind.R;

public class SettingFragmentProfile extends Fragment implements OnClickListener {

	private Button buttonBack;

	public static SettingFragmentProfile newInstance() {
		SettingFragmentProfile fragment = new SettingFragmentProfile();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(
				R.layout.fragment_layout_setting_profile, container, false);
		buttonBack = (Button) rootView.findViewById(R.id.id_btn_back);
		buttonBack.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_btn_back:
			getActivity().onBackPressed();
			break;
		default:
			break;
		}

	}
}

package com.appalmighty.ezefind.setting.fgmt;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.appalmighty.ezefind.R;

public class SettingFragmentAccount extends Fragment implements OnClickListener {

	public static SettingFragmentAccount newInstance() {
		SettingFragmentAccount fragment = new SettingFragmentAccount();
		return fragment;
	}

	private ImageView imageButtonPwd;
	private Button buttonBack;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(
				R.layout.fragment_layout_setting_account, container, false);
		imageButtonPwd = (ImageView) rootView.findViewById(R.id.id_btn_pwd);
		buttonBack = (Button) rootView.findViewById(R.id.id_btn_back);
		buttonBack.setOnClickListener(this);
		imageButtonPwd.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		Fragment fragment = null;

		int id = v.getId();
		switch (id) {
		case R.id.id_btn_pwd:
			fragment = SettingFragmentPassword.newInstance();
			switchToFragment(fragment);
			break;
		case R.id.id_btn_back:
			getActivity().onBackPressed();
			break;

		default:
			break;
		}

	}

	private void switchToFragment(Fragment fragment) {

		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.container, fragment);
		ft.addToBackStack(null);
		ft.commit();

	}
}

package com.appalmighty.ezefind.util;

public class AppConstant {

	public static final String KEY_BAG = "kBag";
	public static final String KEY_BOX = "kBox";
	public static final String KEY_SINGLE_ITEM = "kSingleItem";
	public static final String KEY_BIN = "kBin";

	public static final String KEY_APP = "com.appalmighty.ezefind.APPLICATION";
	public static final String KEY_USER_NAME = "com.appalmighty.ezefind.USERNAME";
	public static final String KEY_USER_ID = "com.appalmighty.ezefind.USERID";
	public static final String KEY_USER_PASSWORD = "com.appalmighty.ezefind.PASSWORD";
	public static final String PICK_GO_TYPE = "com.appalmighty.ezefind.PICK_GO_TYPE";

}

package com.appalmighty.ezefind.net;

public class NetworkConstant {

	public class Base {
		public static final String HOST = "http://beta.brstdev.com";
		public static final String API = HOST + "/ezifind/frontend/web/api/";
	}

	public class Api {
		public static final String LOGIN = "login";
		public static final String REGISTER = "register";
		public static final String RESET = "reset";
		public static final String PACKAGE = "package";
	}

}

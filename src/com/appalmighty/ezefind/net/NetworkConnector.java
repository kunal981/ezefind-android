package com.appalmighty.ezefind.net;

import java.io.IOException;
import java.util.List;

import org.apache.http.NameValuePair;

import com.appalmighty.ezefind.net.NetworkConstant.Api;
import com.appalmighty.ezefind.net.NetworkConstant.Base;

import android.util.Log;

public class NetworkConnector {

	public static final String TAG = NetworkConnector.class.getSimpleName();

	public static String loginAuthentication(List<NameValuePair> nameValuePair) {
		String url = null;
		url = Base.API + Api.LOGIN;

		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;

	}

	public static String registerUser(List<NameValuePair> nameValuePair) {
		String url = null;
		url = Base.API + Api.REGISTER;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;

	}

	public static String forgotPassword(List<NameValuePair> nameValuePair) {
		String url = null;
		url = Base.API + Api.RESET;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;

	}

	public static String saveUserForm(List<NameValuePair> nameValuePair) {
		String url = null;
		url = Base.API + Api.PACKAGE;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;

	}

}

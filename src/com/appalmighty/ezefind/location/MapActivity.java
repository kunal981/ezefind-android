package com.appalmighty.ezefind.location;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.util.Helper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public class MapActivity extends FragmentActivity implements LocationListener,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, OnClickListener {

	private GoogleMap map;
	private static String TAG = "Map";
	// A request to connect to Location Services
	private LocationRequest mLocationRequest;

	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;
	private LatLng latLng;

	private RelativeLayout relContainer, relShare;
	private ImageView imageBtnCancel, imagebtnShare;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		map = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();

		initWaypointContainer();
		locationInitilizer();

	}

	private void initWaypointContainer() {
		relContainer = (RelativeLayout) findViewById(R.id.container_waypoint);
		relShare = (RelativeLayout) findViewById(R.id.layout_share);
		imageBtnCancel = (ImageView) findViewById(R.id.img_btn_cancel);
		imagebtnShare = (ImageView) findViewById(R.id.img_btn_share);
		imagebtnShare.setOnClickListener(this);
		imageBtnCancel.setOnClickListener(this);

	}

	private void locationInitilizer() {

		if (servicesConnected()) {
			LocationManager locationManager = (LocationManager) this
					.getSystemService(Context.LOCATION_SERVICE);
			if (locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
					|| locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				mLocationRequest = LocationRequest.create();
				mLocationRequest
						.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
				mLocationRequest.setInterval(1000);
				mLocationRequest.setFastestInterval(500);
				mLocationClient = new LocationClient(this, this, this);
				mLocationClient.connect();

			} else {
				showSettingsAlert();
			}

		}

	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status

			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getSupportFragmentManager(), TAG);
			}
			return false;
		}
	}

	/**
	 * Show a dialog returned by Google Play services for the connection error
	 * code
	 * 
	 * @param errorCode
	 *            An error code returned from onConnectionFailed
	 */
	private void showErrorDialog(int errorCode) {

		// Get the error dialog from Google Play services
		Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode,
				this, 9000);

		// If Google Play services can provide an error dialog
		if (errorDialog != null) {

			// Create a new DialogFragment in which to show the error dialog
			ErrorDialogFragment errorFragment = new ErrorDialogFragment();

			// Set the dialog in the DialogFragment
			errorFragment.setDialog(errorDialog);

			// Show the error dialog in the DialogFragment
			errorFragment.show(getSupportFragmentManager(), TAG);
		}
	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 * 
		 * @param dialog
		 *            An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
		 * This method must return a Dialog to the DialogFragment.
		 */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
		 * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */

		if (connectionResult.hasResolution()) {
			try {

				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this, 9000);

				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */

			} catch (IntentSender.SendIntentException e) {

				// Log the error
				e.printStackTrace();
			}
		} else {

			// If no resolution is available, display a dialog to the user with
			// the error.
			showErrorDialog(connectionResult.getErrorCode());
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		latLng = getCurrentPosition();
		if (latLng != null) {
			// Move the camera instantly to hamburg with a zoom of 15.
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

			// Zoom in, animating the camera.
			map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
			relContainer.setVisibility(View.VISIBLE);

		}
	}

	@Override
	public void onDisconnected() {

	}

	@Override
	public void onLocationChanged(Location arg0) {

	}

	private LatLng getCurrentPosition() {
		LatLng latLng = null;
		Location loc = getLocation();
		if (loc != null) {
			latLng = new LatLng(loc.getLatitude(), loc.getLongitude()); // vancouver
																		// location
		}
		Log.e(TAG, "Latitude:" + latLng.latitude + " longitude:"
				+ latLng.longitude);
		Helper.ToastUi.print(this, "Latitude:" + latLng.latitude
				+ " longitude:" + latLng.longitude);
		// latLng = new LatLng(49.2500, -123.1000);
		// LatLng latLng = new LatLng(latitude, longitude);

		return latLng;
	}

	public Location getLocation() {
		Location currentLocation = null;
		// If Google Play Services is available
		// Get the current location
		if (mLocationClient.isConnected()) {
			currentLocation = mLocationClient.getLastLocation();
		} else {
			Helper.ToastUi.print(this, "Enable To connect");
		}

		// Display the current location in the UI
		// mLatLng.setText(LocationUtils.getLatLng(this, currentLocation));

		return currentLocation;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.img_btn_cancel:
			relContainer.setVisibility(View.GONE);
			break;
		case R.id.img_btn_share:
			relShare.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}

	}

}

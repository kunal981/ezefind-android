package com.appalmighty.ezefind.viewinventory.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.appalmighty.ezefind.R;

public class OnStartPickListFragment extends Fragment {
	private ListAdapter adapter;
	private ListView listview;
	Button backButton;

	public static OnStartPickListFragment newInstance() {
		OnStartPickListFragment fragment = new OnStartPickListFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_picklist_open,
				container, false);
		listview = (ListView) rootView.findViewById(R.id.listView_pick);
		backButton = (Button) rootView.findViewById(R.id.back_button);

		adapter = new ListAdapter(getActivity());
		listview.setAdapter(adapter);
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		return rootView;
	}

	class ListAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;

		public ListAdapter(Activity activity) {
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 9;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.fragment_on_open_listitems, null);

			}
			return convertView;

		}
	}

}

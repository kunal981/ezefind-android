package com.appalmighty.ezefind.report.act;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.report.fgmt.ReportAllFragment;
import com.appalmighty.ezefind.report.fgmt.ReportCategoryFragment;
import com.appalmighty.ezefind.report.fgmt.ReportDateFragment;
import com.appalmighty.ezefind.report.fgmt.ReportItemFragment;
import com.appalmighty.ezefind.report.fgmt.ReportLocationFragment;
import com.appalmighty.ezefind.report.fgmt.ReportMemberFragment;
import com.appalmighty.ezefind.report.fgmt.ReportValueFragment;

public class ViewReportActivity extends FragmentActivity {

	RelativeLayout relButtonAll, relButtonItem, relButtonMember,
			relButtonCategory, relButtonValue, relButtonLocation,
			relButtonDate;
	Button btnBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_view_report);
		if (savedInstanceState == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.container_report, ReportAllFragment.newInstance())
					.commit();
		}

		relButtonAll = (RelativeLayout) findViewById(R.id.id_report_all);
		relButtonItem = (RelativeLayout) findViewById(R.id.id_report_items);
		relButtonMember = (RelativeLayout) findViewById(R.id.id_report_member);
		relButtonCategory = (RelativeLayout) findViewById(R.id.id_report_cat);
		relButtonValue = (RelativeLayout) findViewById(R.id.id_report_val);
		relButtonLocation = (RelativeLayout) findViewById(R.id.id_report_location);
		relButtonDate = (RelativeLayout) findViewById(R.id.id_report_date);
		btnBack = (Button) findViewById(R.id.back_button);
		relButtonAll.setOnClickListener(new MenuClickListner());
		relButtonItem.setOnClickListener(new MenuClickListner());
		relButtonMember.setOnClickListener(new MenuClickListner());
		relButtonCategory.setOnClickListener(new MenuClickListner());
		relButtonValue.setOnClickListener(new MenuClickListner());
		relButtonLocation.setOnClickListener(new MenuClickListner());
		relButtonDate.setOnClickListener(new MenuClickListner());
		btnBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		setupUiForintent();
	}

	private void setupUiForintent() {
		// TODO Auto-generated method stub
		String stringIntent = getIntent().getStringExtra("key");
		if (stringIntent.equals("history")) {
			initDate();
		} else if (stringIntent.equals("user")) {
			initMember();

		} else {
			relButtonAll.setSelected(true);
		}
	}

	private void initMember() {

		Fragment fragment = ReportMemberFragment.newInstance();
		menuSelection(R.id.id_report_member);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.container_report, fragment).commit();

	}

	private void initDate() {
		Fragment fragment = ReportDateFragment.newInstance();
		menuSelection(R.id.id_report_date);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.container_report, fragment).commit();
	}

	class MenuClickListner implements OnClickListener {

		@Override
		public void onClick(View v) {
			int id = v.getId();
			Fragment fragment = null;
			boolean isSelectedFragment = false;

			switch (id) {
			case R.id.id_report_all:
				if (!relButtonAll.isSelected()) {
					fragment = ReportAllFragment.newInstance();
					menuSelection(id);
					isSelectedFragment = true;
				}
				break;
			case R.id.id_report_items:
				if (!relButtonItem.isSelected()) {
					fragment = ReportItemFragment.newInstance();
					menuSelection(id);
					isSelectedFragment = true;
				}
				break;
			case R.id.id_report_member:
				if (!relButtonMember.isSelected()) {
					fragment = ReportMemberFragment.newInstance();
					menuSelection(id);
					isSelectedFragment = true;
				}
				break;
			case R.id.id_report_cat:
				if (!relButtonCategory.isSelected()) {
					fragment = ReportCategoryFragment.newInstance();
					menuSelection(id);
					isSelectedFragment = true;
				}
				break;
			case R.id.id_report_val:
				if (!relButtonValue.isSelected()) {
					fragment = ReportValueFragment.newInstance();
					menuSelection(id);
					isSelectedFragment = true;
				}
				break;
			case R.id.id_report_location:
				if (!relButtonLocation.isSelected()) {
					fragment = ReportLocationFragment.newInstance();
					menuSelection(id);
					isSelectedFragment = true;
				}
				break;
			case R.id.id_report_date:
				if (!relButtonDate.isSelected()) {
					fragment = ReportDateFragment.newInstance();
					menuSelection(id);
					isSelectedFragment = true;
				}
				break;

			default:
				break;
			}
			if (isSelectedFragment)
				getSupportFragmentManager().beginTransaction()
						.replace(R.id.container_report, fragment).commit();
		}

	}

	private void menuSelection(int id) {

		unselectedAll();
		switch (id) {
		case R.id.id_report_all:
			relButtonAll.setSelected(true);
			break;
		case R.id.id_report_items:
			relButtonItem.setSelected(true);
			break;
		case R.id.id_report_member:
			relButtonMember.setSelected(true);
			break;
		case R.id.id_report_cat:
			relButtonCategory.setSelected(true);
			break;
		case R.id.id_report_val:
			relButtonValue.setSelected(true);
			break;
		case R.id.id_report_location:
			relButtonLocation.setSelected(true);
			break;
		case R.id.id_report_date:
			relButtonDate.setSelected(true);
			break;

		default:
			break;
		}

	}

	private void unselectedAll() {
		relButtonAll.setSelected(false);
		relButtonItem.setSelected(false);
		relButtonMember.setSelected(false);
		relButtonCategory.setSelected(false);
		relButtonValue.setSelected(false);
		relButtonLocation.setSelected(false);
		relButtonDate.setSelected(false);

	}
}

package com.appalmighty.ezefind.report.fgmt;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appalmighty.ezefind.R;

public class ReportLocationFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_view_report_location, container, false);
		return rootView;
	}

	public static Fragment newInstance() {
		ReportLocationFragment fragment = new ReportLocationFragment();
		return fragment;
	}

}

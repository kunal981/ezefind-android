package com.appalmighty.ezefind.report.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.appalmighty.ezefind.R;

public class ReportMemberFragment extends Fragment {
	private GridViewAdapter adapter1;
	private GridView gridView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_view_report_member,
				container, false);
		gridView = (GridView) rootView.findViewById(R.id.id_gridview);
		adapter1 = new GridViewAdapter(getActivity());
		gridView.setAdapter(adapter1);
		return rootView;
	}

	public static Fragment newInstance() {
		ReportMemberFragment fragment = new ReportMemberFragment();
		return fragment;
	}

	class GridViewAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;

		public GridViewAdapter(Activity activity) {
			super();
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 4;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.layout_row_member_items, null);

			}
			return convertView;
		}

	}

}
